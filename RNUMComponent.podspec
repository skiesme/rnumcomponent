Pod::Spec.new do |s|
  s.name         = "RNUMComponent"
  s.version          = '0.1.0'
  s.summary          = 'A short description of RNUMComponent.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/wangzhenzy@outlook.com/RNUMComponent'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'wangzhenzy@outlook.com' => 'wangzhenzy@outlook.com' }
  s.source           = { :git => 'https://gitlab.com/skiesme/rnumcomponent.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://gitlab.com/skiesme/rnumcomponent.git", :tag => "v#{s.version}" }

  s.source_files = 'UMReactBridge/**/*.{h,m}'
  s.dependency "React"
  s.dependency "UMCCommon", "7.1.1"
  s.dependency "UMCShare/UI", "6.9.10"
  s.dependency 'UMCShare/Social/ReducedWeChat', "6.9.10"
  s.dependency 'UMCShare/Social/ReducedQQ', "6.9.10"
  s.dependency 'UMCShare/Social/ReducedSina', "6.9.10"

  # s.subspec 'UMAnalytics' do |ss|
  #   ss.source_files        = 'UMComponents/UMAnalytics.framework/Headers/*.h'
  #   ss.public_header_files = 'UMComponents/UMAnalytics.framework/Headers/*.h'
  #   ss.vendored_frameworks = 'UMComponents/UMAnalytics.framework'
  # end
  
  s.subspec 'UMPush' do |ss|
    ss.source_files        = 'UMComponents/UMPush.framework/Headers/*.h'
    ss.public_header_files = 'UMComponents/UMPush.framework/Headers/*.h'
    ss.vendored_frameworks = 'UMComponents/UMPush.framework'
  end

end
